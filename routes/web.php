<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::group(['middleware' => 'isLoggedIn'], function() {
    Route::get('/', ['as' => 'index', 'uses' => 'HomeController@index']);
    Route::get('/poll/{poll}', ['as' => 'poll', 'uses' =>'HomeController@showPoll']);
    Route::post('/poll/{poll}', ['as' => 'poll.vote', 'uses' =>'HomeController@votePoll']);
    Route::get('/poll/{poll}/results', ['as' => 'poll.results', 'uses' =>'HomeController@showPollResults']);
});

Route::group(['prefix' => 'admin', 'middleware' => 'checkAdmin', 'namespace' => 'Admin'], function(){
    Route::get('/', ['as' => 'admin.home', 'uses' => 'HomeController@index']);
    Route::get('polls/create', ['as' => 'admin.polls.create', 'uses' => 'PollsController@create']);
    Route::get('polls/edit/{poll}', ['as' => 'admin.polls.edit', 'uses' => 'PollsController@edit']);
    Route::post('polls/save/{poll}', ['as' => 'admin.polls.save', 'uses' => 'PollsController@save']);
    Route::get('polls/archived', ['as' => 'admin.polls.archived', 'uses' => 'PollsController@archived']);
    Route::get('polls/archive/{poll}', ['as' => 'admin.polls.archive', 'uses' => 'PollsController@archive']);
    Route::get('polls/restore/{poll}', ['as' => 'admin.polls.restore', 'uses' => 'PollsController@restore']);
    Route::get('polls/delete/{poll}', ['as' => 'admin.polls.delete', 'uses' => 'PollsController@delete']);
    Route::get('polls/results/{poll}', ['as' => 'admin.polls.results', 'uses' => 'PollsController@results']);

    Route::get('users', ['as' => 'admin.users.list', 'uses' => 'UsersController@index']);
    Route::get('users/create', ['as' => 'admin.users.create', 'uses' => 'UsersController@create']);
    Route::get('users/edit/{user}', ['as' => 'admin.users.edit', 'uses' => 'UsersController@edit']);
    Route::post('users/save/{user}', ['as' => 'admin.users.save', 'uses' => 'UsersController@save']);
    Route::get('users/delete/{user}', ['as' => 'admin.users.delete', 'uses' => 'UsersController@delete']);
    Route::get('users/ban/{user}', ['as' => 'admin.users.ban', 'uses' => 'UsersController@ban']);
});
