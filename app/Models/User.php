<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * Defines available user types
     */
    const USER_TYPES = [
        'ADMIN' => 0,
        'USER' => 1
    ];

    /**
     * Defines available statuses
     */
    const STATUS = [
        'INACTIVE' => 0,
        'ACTIVE' => 1
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $fields = [
        'name' => ['type' => 'text'],
        'email' => ['type' => 'text'],
        'password' => ['type' => 'password'],
        'password_confirmation' => ['type' => 'password'],
        'status' => [
            'type' => 'select',
            'values' => self::STATUS
        ],
        'type' => [
            'type' => 'select',
            'values' => self::USER_TYPES
        ],
    ];

    public function getFields()
    {
        return $this->fields;
    }


    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        if(in_array($status, self::STATUS)){
            $this->status = $status;
        }
    }

    public function getType()
    {
        return $this->type;
    }

    public function getBanStatus()
    {
        return $this->ban;
    }

    public function updateBanStatus()
    {
        $this->ban = !$this->ban;
    }

    public function setType($type)
    {
        if(in_array($type, self::USER_TYPES)){
            $this->type = $type;
        }
    }

    public function getTypeName()
    {
        return array_search($this->type, self::USER_TYPES);
    }

    public function hasAdmin()
    {
        return $this->type === self::USER_TYPES['ADMIN'];
    }

    public function answers()
    {
        return $this->hasMany('App\Models\UserAnswers', 'user_id');
    }

    public function hasVotedOnPoll(Polls $poll) {
        $pollAnswerIds = $poll->answers->pluck('id')->toArray();
        $userAnswers = $this->answers->pluck('answer_id')->toArray();

        return count(array_intersect($pollAnswerIds, $userAnswers)) > 0;
    }

    public function votedFor(Polls $poll){
        $pollAnswerIds = $poll->answers->pluck('id')->toArray();
        $userAnswers = $this->answers->pluck('answer_id')->toArray();
        $votedFor = array_intersect($pollAnswerIds, $userAnswers);
        return reset($votedFor);
    }
}
