<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answers extends Model
{
    public $fillable = ['text'];

    public function getText()
    {
        return $this->text;
    }

    public function getId()
    {
        return $this->id;
    }

    public function votes()
    {
    	return $this->hasMany(UserAnswers::class, 'answer_id');
    }
}
