<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Polls extends Model
{

    protected $table = 'polls';

    const STATUS = [
        'INACTIVE' => 0,
        'ACTIVE' => 1
    ];

    const PRIVACY = [
        'PRIVATE' => 0,
        'PUBLIC' => 1
    ];

    protected $fillable = ['title', 'question', 'status', 'privacy'];

    protected $fields = [
        'title' => ['type' => 'text'],
        'question' => ['type' => 'text'],
        'status' => [
            'type' => 'select',
            'values' => self::STATUS
        ],
        'privacy' => [
            'type' => 'select',
            'values' => self::PRIVACY
        ],
    ];

    public function getId()
    {
        return $this->id ?: 0;
    }

    public function getFields()
    {
        return $this->fields;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = title;
    }

    public function getQuestion()
    {
        return $this->question;
    }

    public function setQuestion($question)
    {
        $this->question = $question;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        if(in_array($status, self::STATUS)){
            $this->status = $status;
        }
    }

    public function getPrivacy()
    {
        return $this->privacy;
    }

    public function setPrivacy($privacy)
    {
        if(in_array($privacy, self::PRIVACY)){
            $this->privacy = $privacy;
        }
    }

    public function getArchived()
    {
        return $this->archived;
    }

    public function setArchived()
    {
        $this->archived = !$this->getArchived();
    }

    public function answers()
    {
        return $this->hasMany(Answers::class);
    }

    public function votes()
    {
        return $this->hasManyThrough(UserAnswers::class, Answers::class, 'polls_id', 'answer_id');
    }

}
