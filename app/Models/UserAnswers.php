<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAnswers extends Model
{

    protected $table = 'users_answers';

    protected $fillable = ['user_id', 'answer_id'];

    public function answer()
    {
        return $this->hasOne('Answers');
    }

    public function user()
    {
        return $this->hasOne('User');
    }
}
