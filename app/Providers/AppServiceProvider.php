<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use App\Models\Polls;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Validator::extend('twoanswers', function($attribute, $value, $parameters, $validator) {
            if (!is_array($value)) {
                return false;
            }

            $arr = array_filter($value, function($item) {
                $item = trim($item);
                return !empty($item);
            });
            return (count($arr) >= 2);
        });


        Validator::extend('validAnswer', function($attribute, $value, $parameters, $validator) {
            $pollId = $parameters[0];
            $poll = Polls::find($pollId);
            $availableAnswers = $this->getAnswerIds($poll->answers);
            return in_array($value, $availableAnswers);
        });
    }

    private function getAnswerIds($answers)
    {
        return $answers->pluck('id')->toArray();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
