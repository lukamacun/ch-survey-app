<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRestrictedArea {


    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            session()->flash('message', 'You are not logged in!');
            return redirect()->route('login');
        }

        if (!Auth::user()->hasAdmin()) {
            session()->flash('message', 'You don\'t have permissions');
            return redirect()->route('index');
        }
        return $next($request);
    }
}
