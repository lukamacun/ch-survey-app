<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Polls;
use App\Models\Answers;
use App\Models\UserAnswers;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index', [
            'polls' => Polls::all()
        ]);
    }

    public function showPoll(Polls $poll)
    {
        return view('poll', [
            'poll' => $poll
        ]);
    }

    public function votePoll(Request $request, Polls $poll)
    {
        $this->validate($request, [
            'answer' => 'required|validAnswer:'.$poll->getId()
        ]);
        if(Auth::user()->hasVotedOnPoll($poll)) {
            session()->flash('message', 'You already voted');
            return redirect()->route('poll.vote', ['poll' => $poll]);
        }
        UserAnswers::create([
            'user_id' => Auth::user()->getId(),
            'answer_id' => $request->only('answer')['answer']
        ]);
        return redirect()->route('poll.results', ['poll' => $poll]);
    }

    public function showPollResults(Polls $poll)
    {
        $totalVotes = count($poll->votes);

        return view('results', [
            'poll' => $poll,
            'votes' => $totalVotes
        ]);
    }
}
