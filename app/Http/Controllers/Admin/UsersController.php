<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class UsersController extends Controller
{

    public function index()
    {
        $users = User::all();
        return view('user.index', ['users' => $users]);
    }

    public function edit(User $user)
    {
        return view('user.edit', ['user' => $user]);
    }

    public function save(Request $request, $id)
    {
        $this->validate($request, [
            'name'     => 'string|required',
            'email'    => 'email|required',
            'password' => 'string|confirmed'
        ]);


        $user = new User;

        if ($id) {
            $user = User::find($id);
            if ($user == null) {
                throw new EntityNotFoundException("The requested item was not found in the database.");
            }
        }

        $data = $request->only(['name', 'email', 'password', 'type', 'status']);

        $user->setName($data['name']);
        $user->setEmail($data['email']);
        $user->setType($data['type']);
        $user->setStatus($data['status']);
        if($data['password']) {
            $user->setPassword(Hash::make($data['password']));
        }

        $user->save();

        session()->flash('message', 'User has been successfully saved.');

        return redirect()->route('admin.users.list');

    }

    public function create()
    {
        return view('user.edit', ['user' => new User]);
    }

    public function delete(User $user)
    {
        if (Auth::user() == $user) {
            session()->flash('error', 'You can\'t delete yourself!');
            return redirect()->back();
        }

        $user->delete();
        session()->flash('message', 'User is successfully deleted!');
        return redirect()->route('admin.users.list');
    }

    public function ban(User $user)
    {
        $user->updateBanStatus();
        $user->save();
        return redirect()->route('admin.users.list');
    }


    public function unban(User $user)
    {
        $user->setStatus(User::STATUS_ACTIVE);
        $user->save();
        session()->flash('message', 'User is successfully unbanned!');
        return redirect()->route('admin.users.list');
    }
}
