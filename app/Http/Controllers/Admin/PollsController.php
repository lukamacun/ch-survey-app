<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Polls;
use App\Models\Answers;

class PollsController extends Controller
{
    private $repository;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('polls.index');
    }

    public function create()
    {
        return view('polls.create', [
            'poll' => new Polls
        ]);
    }

    public function edit(Polls $poll)
    {
        return view('polls.create', [
            'poll' => $poll
        ]);
    }

    public function delete(Polls $poll)
    {
        $poll->delete();
        session()->flash('success', 'Poll has been deleted.');
        return redirect()->route('admin.home');
    }

    public function save(Request $request, $id) {
        $this->validate($request, [
            'answer' => 'required|twoanswers',
            'title'  => 'string|required',
            'status' => 'string|required',
        ]);


        $data = $request->except('_token');
        if(!$id){
            $poll = new Polls($data);
        }
        else {
            $poll = new Polls();
        }
        $poll->user_id = \Auth::user()->id;
        $answers = $this->prepareAnswers($data['answer']);
        $poll->save();
        $poll->answers()->delete();
        $poll->answers()->saveMany($answers);
        return redirect()->route('admin.home');
    }

    public function archive(Request $request, Polls $poll)
    {
        $poll->setArchived();
        $poll->save();

        return redirect()->route('admin.home');
    }

    private function prepareAnswers($answers)
    {
        return array_map(function($answer) {
            return new Answers(['text' => $answer]);
        }, $answers);
    }
}
