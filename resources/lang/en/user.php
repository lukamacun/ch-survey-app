<?php

return [
    'user.type.administrator' => 'Administrator',
    'user.status.active' => 'Active',
    'user.status.inactive' => 'Inactive',
];
