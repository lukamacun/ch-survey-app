@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Polls</div>

                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <td>Name</td>
                            <td>Archive</td>
                            <td>Edit</td>
                            <td>Delete</td>
                        </thead>
                        <tbody>
                            @foreach($polls as $poll)
                                <tr>
                                    <td>
                                        {{$poll->title}}
                                    </td>
                                    <td>
                                        @if(!$poll->getArchived())
                                            <a href="{{route('admin.polls.archive', ['id' => $poll->getId()])}}" title="">Archive</a>
                                        @else
                                            <a href="{{route('admin.polls.archive', ['id' => $poll->getId()])}}" title="">Restore</a>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{route('admin.polls.edit', ['id' => $poll->getId()])}}" title="">Edit</a>
                                    </td>
                                    <td>
                                        <a href="{{route('admin.polls.delete', ['id' => $poll->getId()])}}" title="">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
