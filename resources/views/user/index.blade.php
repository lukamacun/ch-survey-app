@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Polls
                    <span class="right"><a href="{{route('admin.users.create')}}" title="">Add new User</a></span>
                </div>
                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <td>Name</td>
                            <td>Email</td>
                            <td>Type</td>
                            <td>Ban</td>
                            <td>Edit</td>
                            <td>Delete</td>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>
                                        {{$user->name}}
                                    </td>
                                    <td>
                                        {{$user->email}}
                                    </td>
                                    <td>
                                        {{$user->getTypeName()}}
                                    </td>
                                    <td>
                                        @if(!$user->getBanStatus())
                                            <a href="{{route('admin.users.ban', ['id' => $user->getId()])}}" title="">Ban</a>
                                        @else
                                            <a href="{{route('admin.users.ban', ['id' => $user->getId()])}}" title="">Unban</a>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{route('admin.users.edit', ['id' => $user->getId()])}}" title="">Edit</a>
                                    </td>
                                    <td>
                                        <a href="{{route('admin.users.delete', ['id' => $user->getId()])}}" title="">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
