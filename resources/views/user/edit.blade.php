@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create new user</div>
                    <div class="panel-body">
                        <form action="{{route('admin.users.save', ['id' => $user->getId() ?: 0])}}" method="post">
                            {!! csrf_field() !!}
                            @foreach($user->getFields() as $name => $config)
                                @if($config['type'] === 'text')
                                    @component('components.text', ['name' => $name, 'value' => $user->{'get'.ucfirst($name)}()]) @endcomponent
                                @elseif($config['type'] === 'select')
                                    @component('components.select', ['name' => $name, 'values' => $config['values'], 'selected' => $user->{'get'.ucfirst($name)}()])@endcomponent
                                @elseif($config['type'] === 'password' && $user->getId() === null)
                                    @component('components.password', ['name' => $name]) @endcomponent
                                @endif
                            @endforeach
                            <br>
                            <button type="button" class="btn js-add-new">Add option</button>
                            <button type="submit" class="btn btn-success">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


