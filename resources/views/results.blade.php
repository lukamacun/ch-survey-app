@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $poll->getTitle() }} results</div>
                    <div class="panel-body">
                        <table class="table">
                            <caption>{{ $poll->getQuestion() }}</caption>
                            <thead>
                            <tr>
                                <th style="width: 50%">Answer</th>
                                <th style="width: 20%">Votes</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($poll->answers as $answer)
                                <tr>
                                    <td>{{ $answer->getText() }}</td>
                                    <td>{{ count($answer->votes) != 0 ? (count($answer->votes) / $votes) * 100 : 0 }}%</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <p>
                            <a href="{{ route('index') }}" class="btn btn-default">Back to polls</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
