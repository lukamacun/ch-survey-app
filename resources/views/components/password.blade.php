<div class="form-group">
    <label for="{{$name}}">{{ isset($label) ? $label : ucfirst($name)}}</label>
    <input class="form-control" type="password" id="{{$name}}" name="{{$name}}" value="{{isset($value) ? $value : ''}}" />
    @if(isset($deletable))
        <button type="button" class="btn btn-danger js-remove-this">Delete</button>
    @endif
</div>
