<label for="{{$name}}">{{ucfirst($name)}}</label>
<select class="form-control" name="{{$name}}" id="{{$name}}">
	@foreach($values as $text => $value)
		<option @if($selected === $value) selected @endif value="{{$value}}">{{$text}}</option>
	@endforeach
</select>
