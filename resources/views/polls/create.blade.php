@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Create new poll</div>
                    <div class="panel-body">
                        <form action="{{route('admin.polls.save', ['id' => $poll->getId()])}}" method="post">
                            {!! csrf_field() !!}
                            @foreach($poll->getFields() as $name => $config)
                                @if($config['type'] === 'text')
                                    @component('components.text', ['name' => $name, 'value' => $poll->{'get'.ucfirst($name)}()]) @endcomponent
                                @elseif($config['type'] === 'select')
                                    @component('components.select', ['name' => $name, 'values' => $config['values'], 'selected' => $poll->{'get'.ucfirst($name)}()])@endcomponent
                                @endif
                            @endforeach
                            <br>
                            <div class="answer-list">
                                @foreach($poll->answers as $index => $answer)
                                    @component('components.text', ['name' => 'answer['.$index.']', 'label' => ($index+1).'. Answer', 'deletable' => true, 'value' => $answer->text])
                                    @endcomponent
                                @endforeach
                            </div>
                            <button type="button" class="btn js-add-new">Add option</button>
                            <button type="submit" class="btn btn-success">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript">
        $('button.js-add-new').on('click', function(event){
            event.preventDefault();
            var numberOfAnswers = document.querySelectorAll('#app [id^="answer["]').length;
            var newOption = document.querySelector('#answer-template').cloneNode(true).content;
            console.log(newOption);
            var newLabel = updateLabel(newOption.querySelector('label'), numberOfAnswers + 1);
            var newInput = updateInput(newOption.querySelector('input'), numberOfAnswers);
            document.querySelector('.answer-list').appendChild(newOption);
        });

        $(document).on('click', 'button.js-remove-this', function(event){
            event.preventDefault();
            console.log($(this).parent('.form-group'))
            $(this).parent('.form-group').remove();
        });

        function updateLabel(label, number){
            var newAttribute = label.getAttribute('for').replace('{number}', number);
            label.innerHTML = label.innerHTML.replace('{number}', number);
            label.setAttribute('for', newAttribute);
        }

        function updateInput(input, number){
            var newAttribute = input.getAttribute('id').replace('{number}', number);
            input.setAttribute('id', newAttribute);
            input.setAttribute('name', newAttribute);
        }

    </script>
@stop

@section('templates')
    <template id="answer-template">
        @component('components.text', ['name' => 'answer[{number}]', 'label' => '{number}. Answer', 'deletable' => true])@endcomponent
    </template>
@stop
