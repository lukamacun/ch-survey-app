@extends('layouts.app')

@section('content')
    <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{$poll->getTitle()}}</div>

                <div class="panel-body">
                    <h2>{{$poll->getQuestion()}}</h2>
                    <form action="{{route('poll.vote', ['poll' => $poll->getId()])}}" method="post" accept-charset="utf-8">
                        {!! csrf_field() !!}
                        @foreach($poll->answers as $index => $answer)
                            <div class="form-group">
                                <label>{{$answer->getText()}}</label>
                                {{Auth::user()->votedFor($poll)}}
                                <input @if(Auth::user()->hasVotedOnPoll($poll)) disabled @endif @if(Auth::user()->votedFor($poll) == $answer->getId()) checked @endif type="radio" name="answer" value="{{$answer->getId()}}" id="answer-{{$index}}">
                            </div>
                        @endforeach
                        @if(!Auth::user()->hasVotedOnPoll($poll))
                        <button class="btn btn-success" type="submit">Vote</button>
                        @else
                            <a href="{{route('poll.results', ['poll' => $poll])}}" title="">See results</a>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
