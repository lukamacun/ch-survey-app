@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Polls</div>

                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <td>Name</td>
                            <td>Total</td>
                        </thead>
                        <tbody>
                            @foreach($polls as $poll)
                                <tr>
                                    <td>
                                        <a href="{{route('poll', ['poll' => $poll->getId()])}}" title="">{{$poll->title}}</a>
                                    </td>
                                    <td>
                                        {{count($poll->votes)}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
