<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'CH Admin',
            'email' => 'polladmin@cloudhorizon.com',
            'password' => Hash::make('polladmin'),
            'type' => User::USER_TYPES['ADMIN'],
            'status' => User::STATUS['ACTIVE'],
        ]);
    }
}
